import os
import re, string
import pandas as pd, numpy as np
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn import metrics
from nbsvm import NbSvmClassifier

data_path = 'data'

train = pd.read_csv(os.path.join(data_path, 'train.csv'))
test = pd.read_csv(os.path.join(data_path, 'test.csv'))
subm = pd.read_csv(os.path.join(data_path, 'sample_submission.csv'))

class_names = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']
train['none'] = 1-train[class_names].max(axis=1)

COMMENT = 'comment_text'
train[COMMENT].fillna("unknown", inplace=True)
test[COMMENT].fillna("unknown", inplace=True)


re_tok = re.compile(f'([{string.punctuation}“”¨«»®´·º½¾¿¡§£₤‘’])')
def tokenize(s): return re_tok.sub(r' \1 ', s).split()

print('Vectorising (TF-IDF)...')
vec = TfidfVectorizer(ngram_range=(1,2), tokenizer=tokenize,
               min_df=3, max_df=0.9, strip_accents='unicode', use_idf=1,
               smooth_idf=1, sublinear_tf=1 )
trn_term_doc = vec.fit_transform(train[COMMENT])
test_term_doc = vec.transform(test[COMMENT])

x = trn_term_doc
test_x = test_term_doc

preds = np.zeros((len(test), len(class_names)))

for i, j in enumerate(class_names):
    print('fit', j)
    m = NbSvmClassifier(C=4, dual=True, n_jobs=-1).fit(x, train[j])
    preds[:,i] = m.predict_proba(test_x)[:,1]

submid = pd.DataFrame({'id': subm["id"]})
submission = pd.concat([submid, pd.DataFrame(preds, columns = class_names)], axis=1)
submission.to_csv('submission.csv', index=False)

