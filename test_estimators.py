# coding: utf-8

#%%

# Imports
import base64
import pandas as pd
import seaborn as sns
import numpy as np
import nltk
import re
import string
import os
import xgboost as xgb
from nltk.corpus import stopwords
from sklearn import metrics
from sklearn.calibration import CalibratedClassifierCV
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, train_test_split, GridSearchCV, cross_val_score
from sklearn.naive_bayes import MultinomialNB, BernoulliNB, GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.svm import LinearSVC
from sklearn.neural_network import BernoulliRBM
from scipy.sparse import hstack
from matplotlib import pyplot as plt
from nbsvm import NbSvmClassifier

#get_ipython().run_line_magic('matplotlib', 'inline')

# In[ ]:

# Load the data with Pandas
data_path = 'data'

train_df = pd.read_csv(os.path.join(data_path, 'train.csv'))
test_df = pd.read_csv(os.path.join(data_path, 'test.csv'))

#%%
class_names = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']
train_df['none'] = 1-train_df[class_names].max(axis=1)

COMMENT = 'comment_text'
train_df[COMMENT].fillna("unknown", inplace=True)
test_df[COMMENT].fillna("unknown", inplace=True)

# In[ ]:

def test_pipeline(df, nlp_pipeline, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    rskf = StratifiedKFold(n_splits=5, random_state=1)
    losses = []
    for train_index, test_index in rskf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        nlp_pipeline.fit(X_train, y_train)
        losses.append(metrics.log_loss(y_test, nlp_pipeline.predict_proba(X_test)))
    print(f'{pipeline_name} kfolds log losses: {str([str(round(x, 3)) for x in sorted(losses)])}')
    print(f'{pipeline_name} mean log loss: {round(pd.np.mean(losses), 3)}')

def test_grid_pipeline(df, nlp_pipeline, parameters=None, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
    grid_search = GridSearchCV(nlp_pipeline, parameters, scoring="neg_log_loss", n_jobs=3, cv=kfold, verbose=2)
    #grid_search = GridSearchCV(nlp_pipeline, parameters, n_jobs=3, cv=kfold, verbose=2)
    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y, stratify=y, test_size=0.25, random_state=80)
    
    grid_result = grid_search.fit(X_train, y_train)

    # summarize results
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))        
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))

def run_pipeline(df, nlp_pipeline):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y, stratify=y, test_size=0.25, random_state=80)
    nlp_pipeline.fit(X_train, y_train)
    print(metrics.classification_report(
        y_valid,
        nlp_pipeline.predict(X_valid)))

#Feature union pipeline kfolds log losses: ['0.459', '0.462', '0.472', '0.476', '0.476']
#Feature union pipeline mean log loss: 0.469

# In[]:

re_tok = re.compile(f'([{string.punctuation}“”¨«»®´·º½¾¿¡§£₤‘’])')
def tokenize(s): return re_tok.sub(r' \1 ', s).split()

def test_estimator(train_df, test_df, class_names, train_features, test_features):
    losses = []
    predictions = {'id': test_df['id']}
    for class_name in class_names:
        train_target = train_df[class_name]
        classifier = LogisticRegression(C=4.0, solver='sag')
        # Total CV loss is -0.055842080876817524
        
        # classifier = NbSvmClassifier(C=4, dual=True, n_jobs=1, solver='liblinear')
        # Total CV loss is -0.05537479200692972

        #classifier = NbSvmClassifier(C=4, dual=False, n_jobs=-1, solver='sag')
        # Total CV loss is -0.05522708406669463

        cv_loss = np.mean(cross_val_score(classifier, train_features, train_target, cv=3, scoring='neg_log_loss', n_jobs=-1))
        losses.append(cv_loss)
        print('CV loss for class {} is {}'.format(class_name, cv_loss))

        classifier.fit(train_features, train_target)
        predictions[class_name] = classifier.predict_proba(test_features)[:, 1]

    print('Total CV loss is {}'.format(np.mean(losses)))

    submission = pd.DataFrame.from_dict(predictions)
    submission.to_csv('submission.csv', index=False)

if __name__ == '__main__':

    # pipe = Pipeline([
    #     # ('cv', CountVectorizer(ngram_range=(1, 2))),
    #     # ('tfidf', TfidfTransformer(use_idf=False)),
    #     #('tfidf', TfidfVectorizer(token_pattern=r'\w{1,}', sublinear_tf=True, ngram_range=(1, 2))),
    #     ('tfidf', TfidfVectorizer(ngram_range=(1,2), tokenizer=tokenize,
    #            min_df=3, max_df=0.9, strip_accents='unicode', use_idf=1,
    #            smooth_idf=1, sublinear_tf=1)),
    #     ('bRBM', BernoulliRBM(random_state=1, verbose=2)),
    #     ('mnb', MultinomialNB(alpha=0.01)),
    # ])

    # params = {
    #     # 'bRBM__learning_rate': [10, 1, 0.1, 0.01, 0.001],
    #     'bRBM__n_components': [2],
    #     'bRBM__n_iter': [5],
    #     # 'bRBM__batch_size': [20, 50, 100],
    # }

    #print(pipe.get_params().keys())
    #test_grid_pipeline(train_df[:500], pipe, parameters=params)
    #test_pipeline(train_df, pipe)
    #run_pipeline(train_df, pipe)

    
    # print('Vectorising (TF-IDF)...')
    # vec = TfidfVectorizer(ngram_range=(1,2), tokenizer=tokenize,
    #             min_df=3, max_df=0.9, strip_accents='unicode', use_idf=1,
    #             smooth_idf=1, sublinear_tf=1 )
    # trn_term_doc = vec.fit_transform(train_df[COMMENT])
    # test_term_doc = vec.transform(test_df[COMMENT])

    from sklearn.externals import joblib
    #joblib.dump(trn_term_doc, os.path.join(data_path, 'trn_term_doc.pkl'))
    #joblib.dump(test_term_doc, os.path.join(data_path, 'test_term_doc.pkl'))
    print('Loading vectorised features...')
    trn_term_doc = joblib.load(os.path.join(data_path, 'trn_term_doc.pkl'))
    test_term_doc = joblib.load(os.path.join(data_path, 'test_term_doc.pkl'))

    # print('Vectorising (TF-IDF char (1, 4))...')
    # char_vectorizer = TfidfVectorizer(
    #     sublinear_tf=True,
    #     strip_accents='unicode',
    #     analyzer='char',
    #     ngram_range=(1, 4),
    #     max_features=20000)
    # char_vectorizer.fit_transform(train_df[COMMENT])
    # train_char_features = char_vectorizer.transform(train_df[COMMENT])
    # test_char_features = char_vectorizer.transform(test_df[COMMENT])    
    #joblib.dump(train_char_features, os.path.join(data_path, 'train_char_features.pkl'))
    #joblib.dump(test_char_features, os.path.join(data_path, 'test_char_features.pkl'))
    train_char_features = joblib.load(os.path.join(data_path, 'train_char_features.pkl'))
    test_char_features = joblib.load(os.path.join(data_path, 'test_char_features.pkl'))

    train_features = hstack((train_char_features, trn_term_doc))
    test_features = hstack((test_char_features, test_term_doc))

    print("Cross-validating...")
    test_estimator(train_df, test_df, class_names, train_features, test_features)