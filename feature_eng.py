import base64
import pandas as pd
import seaborn as sns
import numpy as np
import nltk
import re
import string
import os
import xgboost as xgb
from nltk.corpus import stopwords
from sklearn import metrics
from sklearn.calibration import CalibratedClassifierCV
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, train_test_split, GridSearchCV, cross_val_score
from sklearn.naive_bayes import MultinomialNB, BernoulliNB, GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.svm import LinearSVC
from sklearn.neural_network import BernoulliRBM
from scipy.sparse import hstack
from matplotlib import pyplot as plt
from nbsvm import NbSvmClassifier

data_path = 'data'

train_df = pd.read_csv(os.path.join(data_path, 'train.csv'))
test_df = pd.read_csv(os.path.join(data_path, 'test.csv'))


class_names = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']
train_df['none'] = 1-train_df[class_names].max(axis=1)

COMMENT = 'comment_text'
train_df[COMMENT].fillna("_na_", inplace=True)
test_df[COMMENT].fillna("_na_", inplace=True)

eng_stopwords = set(stopwords.words("english"))
train_df["num_stopwords"] = train_df[COMMENT].apply(lambda x: len([w for w in str(x).lower().split() if w in eng_stopwords]))
test_df["num_stopwords"] = test_df[COMMENT].apply(lambda x: len([w for w in str(x).lower().split() if w in eng_stopwords]))

train_df["length"] = train_df[COMMENT].apply(lambda x: len(x))
test_df["length"] = test_df[COMMENT].apply(lambda x: len(x))

punc = set(f'([{string.punctuation}“”¨«»®´·º½¾¿¡§£₤‘’])')

train_df["num_punctuation"] = train_df[COMMENT].apply(lambda x: sum(x.count(c) for c in punc))
test_df["num_punctuation"] = test_df[COMMENT].apply(lambda x: sum(x.count(c) for c in punc))

train_df["num_exclamation"] = train_df[COMMENT].apply(lambda x: x.count('!'))
test_df["num_exclamation"] = test_df[COMMENT].apply(lambda x: x.count('!'))

def test_estimator(train_df, test_df, class_names, train_features, test_features):
    losses = []
    predictions = {'id': test_df['id']}
    for class_name in class_names:
        train_target = train_df[class_name]
        classifier = LogisticRegression(C=4.0, solver='sag')

        cv_loss = np.mean(cross_val_score(classifier, train_features, train_target, cv=3, scoring='neg_log_loss', n_jobs=-1))
        losses.append(cv_loss)
        print('CV loss for class {} is {}'.format(class_name, cv_loss))

        classifier.fit(train_features, train_target)
        predictions[class_name] = classifier.predict_proba(test_features)[:, 1]

    print('Total CV loss is {}'.format(np.mean(losses)))

    submission = pd.DataFrame.from_dict(predictions)
    submission.to_csv('submission.csv', index=False)

if __name__ == '__main__':

    from sklearn.externals import joblib
    print('Loading vectorised features...')
    trn_term_doc = joblib.load(os.path.join(data_path, 'trn_term_doc.pkl'))
    test_term_doc = joblib.load(os.path.join(data_path, 'test_term_doc.pkl'))

    train_char_features = joblib.load(os.path.join(data_path, 'train_char_features.pkl'))
    test_char_features = joblib.load(os.path.join(data_path, 'test_char_features.pkl'))


    train_feat_eng = [{ 'length': row['length'],
               'num_stopwords': row['num_stopwords'],
               'num_punctuation': row['num_punctuation'],
               'num_exclamation': row['num_exclamation']
               }
            for _, row in train_df.iterrows()]
    test_feat_eng = [{ 'length': row['length'],
               'num_stopwords': row['num_stopwords'],
               'num_punctuation': row['num_punctuation'],
               'num_exclamation': row['num_exclamation']
               }
            for _, row in test_df.iterrows()]
    from sklearn.feature_extraction import DictVectorizer
    vec = DictVectorizer()

    # train_features = hstack((train_char_features, trn_term_doc, vec.fit_transform(train_feat_eng)))
    # test_features = hstack((test_char_features, test_term_doc, vec.fit_transform(test_feat_eng)))
    train_features = hstack((train_char_features, trn_term_doc))
    test_features = hstack((test_char_features, test_term_doc))

    print("Cross-validating...")
    test_estimator(train_df, test_df, class_names, train_features, test_features)
